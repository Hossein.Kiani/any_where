# Any Where
###### Model builder object filtering

How to use:
Push AnyWhereCriteria in your repository with $request argument
And for calling front must send some data by Query string. E.g.

``http://URL?operation=like&first_name=NAME&wallet.amount=100``

**NOTE**: Operand key is optionally and by default is like.  
This package only work with Laravel and Postgresql BD
