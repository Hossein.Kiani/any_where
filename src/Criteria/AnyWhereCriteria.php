<?php

namespace AnyWhere\Criteria;

use AnyWhere\Helpers\AnyWhereHelper;
use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class AnyWhereCriteria.
 *
 * @package namespace App\Criteria;
 */
class AnyWhereCriteria implements CriteriaInterface
{
    protected $data = [];
    protected $operator = 'ilike';
    protected $dataRelationships = [];

    public function __construct(BaseRepository $repository, $request)
    {
        /** @var Model $model*/
        $model = app($repository->model());
        $columns = Schema::getColumnListing($model->table);
        $request = $request->all();
        if(isset($request['operator'])) {
            $this->operator = $request['operator'];
            unset($request['operator']);
        }

        foreach ($request as $key => $item) {
            if(AnyWhereHelper::isColumn($key, $columns)) {
                $this->data[$key] = $item;
            } else if(strstr($key, '_')) {
                if(AnyWhereHelper::isRelation($model, $key)) {
                    if(AnyWhereHelper::isColumnRelation($model, $key)) {
                        $this->dataRelationships[$key] = $item;
                    }
                }
            }
        }
    }

    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        foreach ($this->data as $column => $value) {
            if(strstr($value, ',')) {
                $model = $model->whereIn($column, explode(',', $value));
                continue;
            }

            if(in_array($this->operator, ['=', '>=', '<=', '>', '<', '!='])) {
                $model = $model->where($column, $this->operator, $value);
                continue;
            }

            $model = $model->where($column, 'ilike', "%$value%");
        }

        if(!empty($this->dataRelationships)) {
            foreach ($this->dataRelationships as $dataRelationship => $value) {
                list($relation, $column) = AnyWhereHelper::extractRelationData($dataRelationship);
                $model = $model->whereHas($relation, function ($query) use($column, $value) {
                    if(strstr($value, ',')) {
                        return $query->whereIn($column, explode(',', $value));
                    }

                    if(in_array($this->operator, ['=', '>=', '<=', '>', '<', '!='])) {
                        return $query->where($column, $this->operator, $value);
                    }

                    return $query->where($column, 'ilike', "%$value%");
                });
            }
        }

        return $model;
    }
}
