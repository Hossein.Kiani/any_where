<?php


namespace AnyWhere\Helpers;


use Illuminate\Database\Eloquent\Model;
use ErrorException;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionMethod;

/**
 * Class AnyWhereHelper
 * @package App\Helpers
 */
class AnyWhereHelper
{
    /**
     * @param Model $model
     * @return array
     * @throws \ReflectionException
     */
    public static function relationships(Model $model) {

        $relationships = [];

        foreach((new ReflectionClass($model))->getMethods(ReflectionMethod::IS_PUBLIC) as $method)
        {
            if ($method->class != get_class($model) ||
                !empty($method->getParameters()) ||
                $method->getName() == __FUNCTION__) {
                continue;
            }

            try {
                $return = $method->invoke($model);

                if ($return instanceof Relation) {
                    $relationships[$method->getName()] = [
                        'type' => (new ReflectionClass($return))->getShortName(),
                        'model' => (new ReflectionClass($return->getRelated()))->getName()
                    ];
                }
            } catch(ErrorException $e) {}
        }

        return $relationships;
    }

    /**
     * @param $column
     * @param array $columns
     * @return bool
     */
    public static function isColumn($column, $columns = []) {
        return in_array($column, $columns);
    }

    /**
     * @param Model $model
     * @param $modelColumn
     * @return bool
     */
    public static function isColumnRelation(Model $model, $modelColumn) {
        $data = explode('_', $modelColumn);
        $dataLength = count($data);
        $relations = self::relationships($model);
        if (!array_key_exists($data[0], $relations)) {
            return false;
        }

        $relation = app($relations[$data[0]]['model']);
        $columns = Schema::getColumnListing($relation->table);

        if($dataLength == 2) {
            return in_array($data[1], $columns);
        }

        if($dataLength == 3) {
            if($data[2] == 'id') {
                $data[1] = Str::singular($data[1]);
                return in_array("$data[1]_$data[2]", $columns);
            }

            if(!$sub = app($relations[$data[1]]['model'])) {
                return false;
            }

            $columns = Schema::getColumnListing($sub->table);
            return in_array("$data[2]", $columns);
        }

        return false;
    }

    /**
     * @param Model $model
     * @param $relation
     * @return bool
     * @throws \ReflectionException
     */
    public static function isRelation(Model $model, $relation) {
        $relations = self::relationships($model);
        if(strstr($relation, '_')) {
            $relation = explode('_', $relation)[0];
        }

        return array_key_exists($relation, $relations);
    }

    public static function extractRelationData($data) {
        $data = explode('_', $data);
        if(count($data) == 3) {
            return ["$data[0].$data[1]", $data[2]];
        }

        return $data;
    }
    public static function buildCondition($model) {

    }
}
